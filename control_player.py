#!/usr/bin/python

import os
import time

pipepath = '/tmp/cmdpipe'
keyboardtty = '/dev/tty1'

try:
	cmdpipe = os.open(pipepath, os.O_RDWR | os.O_NONBLOCK)
except:
	os.mkfifo(pipepath)		
	cmdpipe = os.open(pipepath, os.O_RDWR | os.O_NONBLOCK)

kbdin = open(keyboardtty, "r")

while True:
	key = kbdin.readline()[:-1]
	print key
	os.write(cmdpipe, key)
	time.sleep(1)	

#while True:
#	cmd = raw_input("Naechster Befehl: ")
#	print cmd
#	os.write(cmdpipe, cmd+"\n")



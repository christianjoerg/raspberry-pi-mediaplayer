#!/usr/bin/python

import os
import time
import sys
import getopt
import subprocess
from subprocess import Popen, PIPE

# global variables
test = 0
OmxVolume = 0


class Album(object):

	
	def __init__(self, name, playlistpath, cardid):
		self.Name = name
		self.Playlistpath = playlistpath
		self.Cardid = cardid
		
	def play(self, cmdpipe, bckpipe):
		global test
		global OmxVolume

		filehandle = open(self.Playlistpath, "r")

		listofsongs = []
		songsinlist = 0
		musicpath, nothing = os.path.split(self.Playlistpath)
		
		for line in filehandle:
			song = line.rstrip()
			listofsongs.append(song)
			songsinlist = songsinlist + 1
		
		i = 0
		send = ' '
		while i < songsinlist:
			if test == 0:
				Popen('killall omxplayer.bin', stdout=PIPE, stderr=PIPE, shell=True)
				playerInstance = Popen(['omxplayer', musicpath + "/" + listofsongs[i] , '-o', 'both', '--vol', str(OmxVolume)], stdin=PIPE, stdout=PIPE)
			else:
				playerInstance = Popen(['sleep', '30'], stdin=PIPE, stdout=PIPE)

			print "playing: " + musicpath + "/" + listofsongs[i]
			os.write(bckpipe, "playing: " + musicpath + "/" + listofsongs[i] + "\n")

			while True:
				if playerInstance.poll() == 0:
					i = i + 1
					break
			
				try:
					command = os.read(cmdpipe, 256)
					command = command.split('\n')[0]
				except:
					command = 0
					time.sleep(1)

				if command == 'play':
					os.write(bckpipe, "play\n")
					send = ' '
				elif command == 'pause':
					os.write(bckpipe, "pause\n")
					send = ' '
				elif command == 'stop':
					os.write(bckpipe, "stop\n")
					i = songsinlist + 1			# stop playing album
					break 
				elif command == 'seek_fast_rewind':
					#Down key
					send = '\x1B[B'
				elif command == 'seek_rewind':
					#Left key
					send = '\x1B[D'
				elif command == 'seek_forward':
					#Right key
					send = '\x1B[C'
				elif command == 'seek_fast_forward':
					#Up key
					send = '\x1B[A'
				elif command == 'volume_down':
					os.write(bckpipe, "volume_down\n")
					send = '-'
					OmxVolume -= 300
				elif command == 'volume_up':
					os.write(bckpipe, "volume_up\n")
					send = '+'
					OmxVolume += 300
				elif command == 'next':
					os.write(bckpipe, "next\n")
					i = i + 1
					i = i % songsinlist
					break
				elif command == 'last':
					os.write(bckpipe, "last\n")
					i = i - 1
					i = i % songsinlist
					break
				else:
					send = ''
					
				playerInstance.stdin.write(send)
				
				time.sleep(0.1)
			
		#Popen('killall omxplayer.bin', stdout=PIPE, stderr=PIPE, shell=True)	
		filehandle.close()
	#end play
#end class album

def check_media_file(filename):
	if filename.lower().endswith('.mp3'):
		return 1
	elif filename.lower().endswith('.mp4'):
		return 1
	elif filename.lower().endswith('.mpg'):
		return 1
	elif filename.lower().endswith('.m4v'):
		return 1
	else:
		return 0

#end check_media_file

def run_musicplayer(musicdir):
	cmdpipepath = '/tmp/cmdpipe'
	bckpipepath = '/tmp/bckpipe'
	listofalbums = []

	for folderName, subfolders, filenames in os.walk(musicdir):
		for f in filenames:
			# pruefen, ob Folder mindestens eine MP3-Datei enthaelt
			if check_media_file(f) > 0:
				print folderName

				if not os.path.isfile(folderName+'/card.id'):
					print folderName + ": no card.id file"
				else:
					idobj = open(folderName + "/card.id", "r")
					cardid = idobj.readline()[:-1]
					idobj.close()

					nothing, name = os.path.split(folderName)
					listofalbums.append(Album(name, folderName+'/playlist.m3u', cardid))

					# pruefen, ob es noch keine Playlist gibt
					if not os.path.isfile(folderName+'/playlist.m3u'):
						print "no Playlist"
						# Playlist erstellen
						playlistobj = open(folderName + "/playlist.m3u", "w")
						for folderName1, subfolders1, filenames1 in os.walk(folderName):
							for f1 in filenames1:
								if check_media_file(f) > 0:
									playlistobj.write(f1 + "\n")
						playlistobj.close()

				break
		#end for f in filenames

	for album in listofalbums:
		print album.Name + ", " + album.Playlistpath + ", " + album.Cardid

	#open command pipes
	try:
		bckpipe = os.open(bckpipepath, os.O_RDWR | os.O_NONBLOCK)
	except:
		os.mkfifo(bckpipepath)		
		bckpipe = os.open(bckpipepath, os.O_RDWR | os.O_NONBLOCK)
		
	try:
		cmdpipe = os.open(cmdpipepath, os.O_RDONLY | os.O_NONBLOCK)
	except:
		os.mkfifo(cmdpipepath)		
		cmdpipe = os.open(cmdpipepath, os.O_RDONLY | os.O_NONBLOCK)

	print "pipes opened"

	# mainloop
	while True: 
		try:
			command = os.read(cmdpipe, 256)
			command = command.split('\n')[0]
		except:
			command = 0
			time.sleep(1)

		if command == 'play':
			print "play"
		elif command == 'pause':
			print "pause"
		elif command == 'stop\n':
			print "stop"
		elif command == 'exit':
			os.write(bckpipe, "exit\n")
			break
		elif command == 'volume_down':
			os.write(bckpipe, "volume_down\n")
			#OmxVolume -= 300
		elif command == 'volume_up':
			os.write(bckpipe, "volume_up\n")
			#OmxVolume += 300
		else:
		    for album in listofalbums:
			    if command == album.Cardid:
				    # Album abspielen
				    os.write(bckpipe, "play:" + album.Cardid + "\n")
				    album.play(cmdpipe, bckpipe)
				    
		time.sleep(0.1)
    # end of mainloop
	try:
		bkgpipe.close()
	except:
		print "bkgpipe already closed"
	try:
		cmdpipe.close()
	except:
		print "cmdpipe already closed"
#end run_musicplayer

def usage(prog):
    print  "usage: " + prog + " --path=MUSICDIR\n\n" \
         + "options:\n" \
         + "  -p    --path=MUSICDIR   directory where the music is\n" \
         + "  -t    --test            enable test mode\n" \
         + "  -h    --help            show this help\n"
    sys.exit(0)
    
    
# end usage


musicpath = "testmusic"

print sys.argv[0]

try:
    opts, args = getopt.getopt(sys.argv[1:],"htp:", ["help","test","path="])
except:
    print "getopt failed"
    sys.exit(1)
for opt, arg in opts:
	if opt in ("-h", "--help"):
		usage(sys.argv[0])
	elif opt in ("-t", "--test"):
		print "testmode"
		test = 1
	elif opt in ("-p", "--path"):
		musicpath = arg
	else:
		print "wrong argument"
        
run_musicplayer(musicpath)












	
